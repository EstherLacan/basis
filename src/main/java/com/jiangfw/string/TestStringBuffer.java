package com.jiangfw.string;

/**
 * Created by EstherLacan on 2019/2/2.
 */
public class TestStringBuffer {

    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("111");
        sb.append("2222");
        System.out.println(sb.toString());
    }
}
